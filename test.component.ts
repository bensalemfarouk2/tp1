import { Component, OnInit, ɵsetClassMetadata } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {
  Etudiant  = {email :'', classe:'' , Note:'', id:0}
  AjoutEtudiant : any[] = [];
  newEtudiant= false;
  addEtudiant(){
    if (this.Etudiant.email !=''){
      this.Etudiant.id = this.AjoutEtudiant.length + 1;
      this.AjoutEtudiant.push({
        email : this.Etudiant.email,
        classe : this.Etudiant.classe,
        note : this.Etudiant.Note,
        id : this.Etudiant.id
      }
      );
      this.Etudiant.email ='' , this.Etudiant.classe='' , 
      this.Etudiant.Note ='',
      this.Etudiant.id += this.Etudiant.id

    }
  }
   
  constructor() { 
 
  }

  ngOnInit(): void {
  }

}
